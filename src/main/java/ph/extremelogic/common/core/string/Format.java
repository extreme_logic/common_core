package ph.extremelogic.common.core.string;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since April 26, 2014
 */

import java.util.List;
import java.util.Locale;

/**
 * Format class.
 */
public final class Format {
    /**
     * Default constructor.
     */
    private Format() {
        // do nothing constructor
    }
    /**
     * @param input Input string to be processed.
     * @return String with first character capitalized.
     */
    public static String firstCharacterUpper(final String input) {
        StringBuilder tmp = new StringBuilder();
        String delims = "[ ]+";
        String[] tokens = input.split(delims);
        Locale locale = Locale.getDefault();

        for (int index = 0; index < tokens.length; index++) {
            tmp.append(Character.toUpperCase(tokens[index].charAt(0)));
            tmp.append(tokens[index].substring(1).toLowerCase(locale));
            tmp.append(StringConstant.BLANK_CHAR);
        }

        return tmp.toString().trim();
    }
    
    public static String appendList(List <String>list, String appender) {
        StringBuilder sb = new StringBuilder();
        int len;
        for(String s : list) {
            sb.append(s);
            sb.append(appender);
        }
        len = sb.toString().length();
        return sb.toString().substring(0, len - appender.length());
    }

    /**
     * @param input String to be processed.
     * @return first word.
     */
    public static String getFirstWord(final String input) {
        if (input.contains(StringConstant.BLANK_CHAR)) {
            String delims = "[ ]+";
            String[] tokens = input.split(delims);

            return tokens[0];
        } else {
            return input;
        }
    }

    /**
     * @param str Original string with last character.
     * @return String without last character.
     */
    public static String removeLastChar(final String str) {
        return str.substring(0,str.length()-1);
    }
}

