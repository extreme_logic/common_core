package ph.extremelogic.common.core.string;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since April 26, 2014
 */

public final class StringConstant {
    /**
     * Hide default constructor.
     */
    private StringConstant() {
        // Do nothing constructor.
    }
    /**
     * Blank character represented as a string.
     */
    public static final String BLANK_CHAR = " ";

    /**
     * Empty String.
     */
    public static final String EMPTY = "";
}
