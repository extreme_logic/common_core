package ph.extremelogic.common.core.time.chinese;

public class Animal {
    public final static String OX = "Ox";
    public final static String DRAGON = "Dragon";
    public final static String MONKEY = "Monkey";
    public final static String DOG = "Dog";
    public final static String RAT = "Rat";
    public final static String GOAT = "Goat";
    public final static String SNAKE = "Snake";
    public final static String PIG = "Pig";
    public final static String TIGER = "Tiger";
    public final static String HORSE = "Horse";
    public final static String RABBIT = "Rabbit";
    public final static String ROOSTER = "Rooster";
}
