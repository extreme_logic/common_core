package ph.extremelogic.common.core.time;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since April 26, 2014
 */

import java.util.Date;

public class DateDiff {
    /**
     * Default constructor.
     */
    public DateDiff() {
        // Intentionally blank.
    }
    
    public DateDiff(final Date iDate,
                    final Date tDate) {
        setInitialDate(iDate);
        setTargetDate(tDate);
    }

    /**
     * @param initialDate the initialDate to set
     */
    public void setInitialDate(final Date initialDate) {
        this.initialDate = initialDate;
    }

    /**
     * @param targetDate the targetDate to set
     */
    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    /**
     * Initial date.
     */
    private Date initialDate;
    
    /**
     * Target date.
     */
    private Date targetDate;
    
    public int getTotalDays() {
        return 0;
        
    }
}