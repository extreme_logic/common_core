package ph.extremelogic.common.core.time;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since April 26, 2014
 */

import static ph.extremelogic.common.core.time.DateUtil.getDateFromString;

import java.util.ArrayList;
import java.util.Date;

import ph.extremelogic.common.core.time.chinese.Animal;
import ph.extremelogic.common.core.time.chinese.AnimalCompatibility;

public class ChineseZodiac {    
    private static ArrayList<ChineseNewYear> data;
    private static String DATE_FORMAT;
    
    static {
        DATE_FORMAT = "MMMM d, yyyy";
        data = new ArrayList<ChineseNewYear> ();
        data.add(new ChineseNewYear(getDateFromString("February 5, 1924", DATE_FORMAT), getDateFromString("January 23, 1925", DATE_FORMAT),"Yang Wood ", "Rat"));
        data.add(new ChineseNewYear(getDateFromString("January 24, 1925", DATE_FORMAT), getDateFromString("February 12, 1926", DATE_FORMAT),"Yin Wood ", "Ox"));
        data.add(new ChineseNewYear(getDateFromString("February 13, 1926", DATE_FORMAT), getDateFromString("February 1, 1927", DATE_FORMAT),"Yang Fire ", "Tiger"));
        data.add(new ChineseNewYear(getDateFromString("February 2, 1927", DATE_FORMAT), getDateFromString("January 22, 1928", DATE_FORMAT),"Yin Fire ", "Rabbit"));
        data.add(new ChineseNewYear(getDateFromString("January 23, 1928", DATE_FORMAT), getDateFromString("February 9, 1929", DATE_FORMAT),"Yang Earth ", "Dragon"));
        data.add(new ChineseNewYear(getDateFromString("February 10, 1929", DATE_FORMAT), getDateFromString("January 29, 1930", DATE_FORMAT),"Yin Earth ", "Snake"));
        data.add(new ChineseNewYear(getDateFromString("January 30, 1930", DATE_FORMAT), getDateFromString("February 16, 1931", DATE_FORMAT),"Yang Metal ", "Horse"));
        data.add(new ChineseNewYear(getDateFromString("February 17, 1931", DATE_FORMAT), getDateFromString("February 5, 1932", DATE_FORMAT),"Yin Metal ", "Goat"));
        data.add(new ChineseNewYear(getDateFromString("February 6, 1932", DATE_FORMAT), getDateFromString("January 25, 1933", DATE_FORMAT),"Yang Water ", "Monkey"));
        data.add(new ChineseNewYear(getDateFromString("January 26, 1933", DATE_FORMAT), getDateFromString("February 13, 1934", DATE_FORMAT),"Yin Water ", "Rooster"));
        data.add(new ChineseNewYear(getDateFromString("February 14, 1934", DATE_FORMAT), getDateFromString("February 3, 1935", DATE_FORMAT),"Yang Wood ", "Dog"));
        data.add(new ChineseNewYear(getDateFromString("February 4, 1935", DATE_FORMAT), getDateFromString("January 23, 1936", DATE_FORMAT),"Yin Wood ", "Pig"));
        data.add(new ChineseNewYear(getDateFromString("January 24, 1936", DATE_FORMAT), getDateFromString("February 10, 1937", DATE_FORMAT),"Yang Fire ", "Rat"));
        data.add(new ChineseNewYear(getDateFromString("February 11, 1937", DATE_FORMAT), getDateFromString("January 30, 1938", DATE_FORMAT),"Yin Fire ", "Ox"));
        data.add(new ChineseNewYear(getDateFromString("January 31, 1938", DATE_FORMAT), getDateFromString("February 18, 1939", DATE_FORMAT),"Yang Earth ", "Tiger"));
        data.add(new ChineseNewYear(getDateFromString("February 19, 1939", DATE_FORMAT), getDateFromString("February 7, 1940", DATE_FORMAT),"Yin Earth ", "Rabbit"));
        data.add(new ChineseNewYear(getDateFromString("February 8, 1940", DATE_FORMAT), getDateFromString("January 26, 1941", DATE_FORMAT),"Yang Metal ", "Dragon"));
        data.add(new ChineseNewYear(getDateFromString("January 27, 1941", DATE_FORMAT), getDateFromString("February 14, 1942", DATE_FORMAT),"Yin Metal ", "Snake"));
        data.add(new ChineseNewYear(getDateFromString("February 15, 1942", DATE_FORMAT), getDateFromString("February 4, 1943", DATE_FORMAT),"Yang Water ", "Horse"));
        data.add(new ChineseNewYear(getDateFromString("February 5, 1943", DATE_FORMAT), getDateFromString("January 24, 1944", DATE_FORMAT),"Yin Water ", "Goat"));
        data.add(new ChineseNewYear(getDateFromString("January 25, 1944", DATE_FORMAT), getDateFromString("February 12, 1945", DATE_FORMAT),"Yang Wood ", "Monkey"));
        data.add(new ChineseNewYear(getDateFromString("February 13, 1945", DATE_FORMAT), getDateFromString("February 1, 1946", DATE_FORMAT),"Yin Wood ", "Rooster"));
        data.add(new ChineseNewYear(getDateFromString("February 2, 1946", DATE_FORMAT), getDateFromString("January 21, 1947", DATE_FORMAT),"Yang Fire ", "Dog"));
        data.add(new ChineseNewYear(getDateFromString("January 22, 1947", DATE_FORMAT), getDateFromString("February 9, 1948", DATE_FORMAT),"Yin Fire ", "Pig"));
        data.add(new ChineseNewYear(getDateFromString("February 10, 1948", DATE_FORMAT), getDateFromString("January 28, 1949", DATE_FORMAT),"Yang Earth ", "Rat"));
        data.add(new ChineseNewYear(getDateFromString("January 29, 1949", DATE_FORMAT), getDateFromString("February 16, 1950", DATE_FORMAT),"Yin Earth ", "Ox"));
        data.add(new ChineseNewYear(getDateFromString("February 17, 1950", DATE_FORMAT), getDateFromString("February 5, 1951", DATE_FORMAT),"Yang Metal ", "Tiger"));
        data.add(new ChineseNewYear(getDateFromString("February 6, 1951", DATE_FORMAT), getDateFromString("January 26, 1952", DATE_FORMAT),"Yin Metal ", "Rabbit"));
        data.add(new ChineseNewYear(getDateFromString("January 27, 1952", DATE_FORMAT), getDateFromString("February 13, 1953", DATE_FORMAT),"Yang Water ", "Dragon"));
        data.add(new ChineseNewYear(getDateFromString("February 14, 1953", DATE_FORMAT), getDateFromString("February 2, 1954", DATE_FORMAT),"Yin Water ", "Snake"));
        data.add(new ChineseNewYear(getDateFromString("February 3, 1954", DATE_FORMAT), getDateFromString("January 23, 1955", DATE_FORMAT),"Yang Wood ", "Horse"));
        data.add(new ChineseNewYear(getDateFromString("January 24, 1955", DATE_FORMAT), getDateFromString("February 11, 1956", DATE_FORMAT),"Yin Wood ", "Goat"));
        data.add(new ChineseNewYear(getDateFromString("February 12, 1956", DATE_FORMAT), getDateFromString("January 30, 1957", DATE_FORMAT), "Yang Fire ", "Monkey"));
        data.add(new ChineseNewYear(getDateFromString("January 31, 1957", DATE_FORMAT), getDateFromString("February 17, 1958", DATE_FORMAT),"Yin Fire ", "Rooster"));
        data.add(new ChineseNewYear(getDateFromString("February 18, 1958", DATE_FORMAT), getDateFromString("February 7, 1959", DATE_FORMAT),"Yang Earth ", "Dog"));
        data.add(new ChineseNewYear(getDateFromString("February 8, 1959", DATE_FORMAT), getDateFromString("January 27, 1960", DATE_FORMAT),"Yin Earth ", "Pig"));
        data.add(new ChineseNewYear(getDateFromString("January 28, 1960", DATE_FORMAT), getDateFromString("February 14, 1961", DATE_FORMAT),"Yang Metal ", "Rat"));
        data.add(new ChineseNewYear(getDateFromString("February 15, 1961", DATE_FORMAT), getDateFromString("February 4, 1962", DATE_FORMAT),"Yin Metal ", "Ox"));
        data.add(new ChineseNewYear(getDateFromString("February 5, 1962", DATE_FORMAT), getDateFromString("January 24, 1963", DATE_FORMAT),"Yang Water ", "Tiger"));
        data.add(new ChineseNewYear(getDateFromString("January 25, 1963", DATE_FORMAT), getDateFromString("February 12, 1964", DATE_FORMAT),"Yin Water ", "Rabbit"));
        data.add(new ChineseNewYear(getDateFromString("February 13, 1964", DATE_FORMAT), getDateFromString("February 1, 1965", DATE_FORMAT),"Yang Wood ", "Dragon"));
        data.add(new ChineseNewYear(getDateFromString("February 2, 1965", DATE_FORMAT), getDateFromString("January 20, 1966", DATE_FORMAT),"Yin Wood ", "Snake"));
        data.add(new ChineseNewYear(getDateFromString("January 21, 1966", DATE_FORMAT), getDateFromString("February 8, 1967", DATE_FORMAT),"Yang Fire ", "Horse"));
        data.add(new ChineseNewYear(getDateFromString("February 9, 1967", DATE_FORMAT), getDateFromString("January 29, 1968", DATE_FORMAT),"Yin Fire ", "Goat"));
        data.add(new ChineseNewYear(getDateFromString("January 30, 1968", DATE_FORMAT), getDateFromString("February 16, 1969", DATE_FORMAT),"Yang Earth ", "Monkey"));
        data.add(new ChineseNewYear(getDateFromString("February 17, 1969", DATE_FORMAT), getDateFromString("February 5, 1970", DATE_FORMAT),"Yin Earth ", "Rooster"));
        data.add(new ChineseNewYear(getDateFromString("February 6, 1970", DATE_FORMAT), getDateFromString("January 26, 1971", DATE_FORMAT),"Yang Metal ", "Dog"));
        data.add(new ChineseNewYear(getDateFromString("January 27, 1971", DATE_FORMAT), getDateFromString("February 14, 1972", DATE_FORMAT),"Yin Metal ", "Pig"));
        data.add(new ChineseNewYear(getDateFromString("February 15, 1972", DATE_FORMAT), getDateFromString("February 2, 1973", DATE_FORMAT),"Yang Water ", "Rat"));
        data.add(new ChineseNewYear(getDateFromString("February 3, 1973", DATE_FORMAT), getDateFromString("January 22, 1974", DATE_FORMAT),"Yin Water ", "Ox"));
        data.add(new ChineseNewYear(getDateFromString("January 23, 1974", DATE_FORMAT), getDateFromString("February 10, 1975", DATE_FORMAT),"Yang Wood ", "Tiger"));
        data.add(new ChineseNewYear(getDateFromString("February 11, 1975", DATE_FORMAT), getDateFromString("January 30, 1976", DATE_FORMAT),"Yin Wood ", "Rabbit"));
        data.add(new ChineseNewYear(getDateFromString("January 31, 1976", DATE_FORMAT), getDateFromString("February 17, 1977", DATE_FORMAT),"Yang Fire ", "Dragon"));
        data.add(new ChineseNewYear(getDateFromString("February 18, 1977", DATE_FORMAT), getDateFromString("February 6, 1978", DATE_FORMAT),"Yin Fire ", "Snake"));
        data.add(new ChineseNewYear(getDateFromString("February 7, 1978", DATE_FORMAT), getDateFromString("January 27, 1979", DATE_FORMAT),"Yang Earth ", "Horse"));
        data.add(new ChineseNewYear(getDateFromString("January 28, 1979", DATE_FORMAT), getDateFromString("February 15, 1980", DATE_FORMAT),"Yin Earth ", "Goat"));
        data.add(new ChineseNewYear(getDateFromString("February 16, 1980", DATE_FORMAT), getDateFromString("February 4, 1981", DATE_FORMAT),"Yang Metal ", "Monkey"));
        data.add(new ChineseNewYear(getDateFromString("February 5, 1981", DATE_FORMAT), getDateFromString("January 24, 1982", DATE_FORMAT),"Yin Metal ", "Rooster"));
        data.add(new ChineseNewYear(getDateFromString("January 25, 1982", DATE_FORMAT), getDateFromString("February 12, 1983", DATE_FORMAT),"Yang Water ", "Dog"));
        data.add(new ChineseNewYear(getDateFromString("February 13, 1983", DATE_FORMAT), getDateFromString("February 1, 1984", DATE_FORMAT),"Yin Water ", "Pig"));
        data.add(new ChineseNewYear(getDateFromString("February 2, 1984", DATE_FORMAT), getDateFromString("February 19, 1985", DATE_FORMAT), "Yang Wood ", "Rat"));
        data.add(new ChineseNewYear(getDateFromString("February 20, 1985", DATE_FORMAT), getDateFromString("February 8, 1986", DATE_FORMAT), "Yin Wood ", "Ox"));
        data.add(new ChineseNewYear(getDateFromString("February 9, 1986", DATE_FORMAT), getDateFromString("January 28, 1987", DATE_FORMAT), "Yang Fire ", "Tiger"));
        data.add(new ChineseNewYear(getDateFromString("January 29, 1987", DATE_FORMAT), getDateFromString("February 16, 1988", DATE_FORMAT), "Yin Fire ", "Rabbit"));
        data.add(new ChineseNewYear(getDateFromString("February 17, 1988", DATE_FORMAT), getDateFromString("February 5, 1989", DATE_FORMAT), "Yang Earth ", "Dragon"));
        data.add(new ChineseNewYear(getDateFromString("February 6, 1989", DATE_FORMAT), getDateFromString("January 26, 1990", DATE_FORMAT), "Yin Earth ", "Snake"));
        data.add(new ChineseNewYear(getDateFromString("January 27, 1990", DATE_FORMAT), getDateFromString("February 14, 1991", DATE_FORMAT), "Yang Metal ", "Horse"));
        data.add(new ChineseNewYear(getDateFromString("February 15, 1991", DATE_FORMAT), getDateFromString("February 3, 1992", DATE_FORMAT), "Yin Metal ", "Goat"));
        data.add(new ChineseNewYear(getDateFromString("February 4, 1992", DATE_FORMAT), getDateFromString("January 22, 1993", DATE_FORMAT), "Yang Water ", "Monkey"));
        data.add(new ChineseNewYear(getDateFromString("January 23, 1993", DATE_FORMAT), getDateFromString("February 9, 1994", DATE_FORMAT), "Yin Water ", "Rooster"));
        data.add(new ChineseNewYear(getDateFromString("February 10, 1994", DATE_FORMAT), getDateFromString("January 30, 1995", DATE_FORMAT), "Yang Wood ", "Dog"));
        data.add(new ChineseNewYear(getDateFromString("January 31, 1995", DATE_FORMAT), getDateFromString("February 18, 1996", DATE_FORMAT), "Yin Wood ", "Pig"));
        data.add(new ChineseNewYear(getDateFromString("February 19, 1996", DATE_FORMAT), getDateFromString("February 6, 1997", DATE_FORMAT), "Yang Fire ", "Rat"));
        data.add(new ChineseNewYear(getDateFromString("February 7, 1997", DATE_FORMAT), getDateFromString("January 27, 1998", DATE_FORMAT), "Yin Fire ", "Ox"));
        data.add(new ChineseNewYear(getDateFromString("January 28, 1998", DATE_FORMAT), getDateFromString("February 15, 1999", DATE_FORMAT), "Yang Earth ", "Tiger"));
        data.add(new ChineseNewYear(getDateFromString("February 16, 1999", DATE_FORMAT), getDateFromString("February 4, 2000", DATE_FORMAT), "Yin Earth ", "Rabbit"));
        data.add(new ChineseNewYear(getDateFromString("February 5, 2000", DATE_FORMAT), getDateFromString("January 23, 2001", DATE_FORMAT), "Yang Metal ", "Dragon"));
        data.add(new ChineseNewYear(getDateFromString("January 24, 2001", DATE_FORMAT), getDateFromString("February 11, 2002", DATE_FORMAT), "Yin Metal ", "Snake"));
        data.add(new ChineseNewYear(getDateFromString("February 12, 2002", DATE_FORMAT), getDateFromString("January 31, 2003", DATE_FORMAT), "Yang Water ", "Horse"));
        data.add(new ChineseNewYear(getDateFromString("February 1, 2003", DATE_FORMAT), getDateFromString("January 21, 2004", DATE_FORMAT), "Yin Water ", "Goat"));
        data.add(new ChineseNewYear(getDateFromString("January 22, 2004", DATE_FORMAT), getDateFromString("February 8, 2005", DATE_FORMAT), "Yang Wood ", "Monkey"));
        data.add(new ChineseNewYear(getDateFromString("February 9, 2005", DATE_FORMAT), getDateFromString("January 28, 2006", DATE_FORMAT), "Yin Wood ", "Rooster"));
        data.add(new ChineseNewYear(getDateFromString("January 29, 2006", DATE_FORMAT), getDateFromString("February 17, 2007", DATE_FORMAT), "Yang Fire ", "Dog"));
        data.add(new ChineseNewYear(getDateFromString("February 18, 2007", DATE_FORMAT), getDateFromString("February 6, 2008", DATE_FORMAT), "Yin Fire ", "Pig"));
        data.add(new ChineseNewYear(getDateFromString("February 7, 2008", DATE_FORMAT), getDateFromString("January 25, 2009", DATE_FORMAT), "Yang Earth ", "Rat"));
        data.add(new ChineseNewYear(getDateFromString("January 26, 2009", DATE_FORMAT), getDateFromString("February 13, 2010", DATE_FORMAT), "Yin Earth ", "Ox"));
        data.add(new ChineseNewYear(getDateFromString("February 14, 2010", DATE_FORMAT), getDateFromString("February 2, 2011", DATE_FORMAT), "Yang Metal ", "Tiger"));
        data.add(new ChineseNewYear(getDateFromString("February 3, 2011", DATE_FORMAT), getDateFromString("January 22, 2012", DATE_FORMAT), "Yin Metal ", "Rabbit"));
        data.add(new ChineseNewYear(getDateFromString("January 23, 2012", DATE_FORMAT), getDateFromString("February 9, 2013", DATE_FORMAT), "Yang Water ", "Dragon"));
        data.add(new ChineseNewYear(getDateFromString("February 10, 2013", DATE_FORMAT), getDateFromString("January 30, 2014", DATE_FORMAT), "Yin Water ", "Snake"));
        data.add(new ChineseNewYear(getDateFromString("January 31, 2014", DATE_FORMAT), getDateFromString("February 18, 2015", DATE_FORMAT), "Yang Wood ", "Horse"));
        data.add(new ChineseNewYear(getDateFromString("February 19, 2015", DATE_FORMAT), getDateFromString("February 7, 2016", DATE_FORMAT), "Yin Wood ", "Goat"));
        data.add(new ChineseNewYear(getDateFromString("February 8, 2016", DATE_FORMAT), getDateFromString("January 27, 2017", DATE_FORMAT), "Yang Fire ", "Monkey"));
        data.add(new ChineseNewYear(getDateFromString("January 28, 2017", DATE_FORMAT), getDateFromString("February 15, 2018", DATE_FORMAT), "Yin Fire ", "Rooster"));
        data.add(new ChineseNewYear(getDateFromString("February 16, 2018", DATE_FORMAT), getDateFromString("February 4, 2019", DATE_FORMAT), "Yang Earth ", "Dog"));
        data.add(new ChineseNewYear(getDateFromString("February 5, 2019", DATE_FORMAT), getDateFromString("January 24, 2020", DATE_FORMAT), "Yin Earth ", "Pig"));
        data.add(new ChineseNewYear(getDateFromString("January 25, 2020", DATE_FORMAT), getDateFromString("February 11, 2021", DATE_FORMAT), "Yang Metal ", "Rat"));
        data.add(new ChineseNewYear(getDateFromString("February 12, 2021", DATE_FORMAT), getDateFromString("January 31, 2022", DATE_FORMAT), "Yin Metal ", "Ox"));
        data.add(new ChineseNewYear(getDateFromString("February 1, 2022", DATE_FORMAT), getDateFromString("January 21, 2023", DATE_FORMAT), "Yang Water ", "Tiger"));
        data.add(new ChineseNewYear(getDateFromString("January 22, 2023", DATE_FORMAT), getDateFromString("February 9, 2024", DATE_FORMAT), "Yin Water ", "Rabbit"));
        data.add(new ChineseNewYear(getDateFromString("February 10, 2024", DATE_FORMAT), getDateFromString("January 28, 2025", DATE_FORMAT), "Yang Wood ", "Dragon"));
        data.add(new ChineseNewYear(getDateFromString("January 29, 2025", DATE_FORMAT), getDateFromString("February 16, 2026", DATE_FORMAT), "Yin Wood ", "Snake"));
        data.add(new ChineseNewYear(getDateFromString("February 17, 2026", DATE_FORMAT), getDateFromString("February 5, 2027", DATE_FORMAT), "Yang Fire ", "Horse"));
        data.add(new ChineseNewYear(getDateFromString("February 6, 2027", DATE_FORMAT), getDateFromString("January 25, 2028", DATE_FORMAT), "Yin Fire ", "Goat"));
        data.add(new ChineseNewYear(getDateFromString("January 26, 2028", DATE_FORMAT), getDateFromString("February 12, 2029", DATE_FORMAT), "Yang Earth ", "Monkey"));
        data.add(new ChineseNewYear(getDateFromString("February 13, 2029", DATE_FORMAT), getDateFromString("February 2, 2030", DATE_FORMAT), "Yin Earth ", "Rooster"));
        data.add(new ChineseNewYear(getDateFromString("February 3, 2030", DATE_FORMAT), getDateFromString("January 22, 2031", DATE_FORMAT), "Yang Metal ", "Dog"));
        data.add(new ChineseNewYear(getDateFromString("January 23, 2031", DATE_FORMAT), getDateFromString("February 10, 2032", DATE_FORMAT), "Yin Metal ", "Pig"));
        data.add(new ChineseNewYear(getDateFromString("February 11, 2032", DATE_FORMAT), getDateFromString("January 30, 2033", DATE_FORMAT), "Yang Water ", "Rat"));
        data.add(new ChineseNewYear(getDateFromString("January 31, 2033", DATE_FORMAT), getDateFromString("February 18, 2034", DATE_FORMAT), "Yin Water ", "Ox"));
        data.add(new ChineseNewYear(getDateFromString("February 19, 2034", DATE_FORMAT), getDateFromString("February 7, 2035", DATE_FORMAT), "Yang Wood ", "Tiger"));
        data.add(new ChineseNewYear(getDateFromString("February 8, 2035", DATE_FORMAT), getDateFromString("January 27, 2036", DATE_FORMAT), "Yin Wood ", "Rabbit"));
        data.add(new ChineseNewYear(getDateFromString("January 28, 2036", DATE_FORMAT), getDateFromString("February 14, 2037", DATE_FORMAT), "Yang Fire ", "Dragon"));
        data.add(new ChineseNewYear(getDateFromString("February 15, 2037", DATE_FORMAT), getDateFromString("February 3, 2038", DATE_FORMAT), "Yin Fire ", "Snake"));
        data.add(new ChineseNewYear(getDateFromString("February 4, 2038", DATE_FORMAT), getDateFromString("January 23, 2039", DATE_FORMAT), "Yang Earth ", "Horse"));
        data.add(new ChineseNewYear(getDateFromString("January 24, 2039", DATE_FORMAT), getDateFromString("February 11, 2040", DATE_FORMAT), "Yin Earth ", "Goat"));
        data.add(new ChineseNewYear(getDateFromString("February 12, 2040", DATE_FORMAT), getDateFromString("January 31, 2041", DATE_FORMAT), "Yang Metal ", "Monkey"));
        data.add(new ChineseNewYear(getDateFromString("February 1, 2041", DATE_FORMAT), getDateFromString("January 21, 2042", DATE_FORMAT), "Yin Metal ", "Rooster"));
        data.add(new ChineseNewYear(getDateFromString("January 22, 2042", DATE_FORMAT), getDateFromString("February 9, 2043", DATE_FORMAT), "Yang Water ", "Dog"));
        data.add(new ChineseNewYear(getDateFromString("February 10, 2043", DATE_FORMAT), getDateFromString("January 29, 2044", DATE_FORMAT), "Yin Water ", "Pig"));  
    }
 
    /**
     * Default constructor.
     */
    public ChineseZodiac() {
        // do nothing constructor
  }
    public final static int BASE_YEAR = 1900;
    public final static int MIN_YEAR = 1924;
    public final static int MAX_YEAR = 2043;
    
    public static boolean isYearValid(int year) {
        return (year >= MIN_YEAR && year <= MAX_YEAR);
    }
    
    public static ChineseNewYear getZodiacInfo(Date date) {
        ChineseNewYear info = null;
        for(ChineseNewYear cny : data) {
            if (DateUtil.isDateBetween(cny.getBeginDate(), cny.getEndDate(), date)) {
                info = cny;
                break;
            }
        }
        return info;
    }
    
    public static AnimalCompatibility getRatCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.OX);
        bm.addBestMatch(Animal.DRAGON);
        bm.addBestMatch(Animal.MONKEY);
        
        bm.addMatch(Animal.DOG);
        bm.addMatch(Animal.RAT);
        bm.addMatch(Animal.GOAT);
        bm.addMatch(Animal.SNAKE);
        bm.addMatch(Animal.PIG);
        bm.addMatch(Animal.TIGER);
        
        bm.addNoMatch(Animal.HORSE);
        bm.addNoMatch(Animal.RABBIT);
        bm.addNoMatch(Animal.ROOSTER);
        
        return bm;
    }
    
    public static AnimalCompatibility getOxCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.RAT);
        bm.addBestMatch(Animal.ROOSTER);
        bm.addBestMatch(Animal.PIG);
        bm.addBestMatch(Animal.SNAKE);
        
        bm.addMatch(Animal.OX);
        bm.addMatch(Animal.TIGER);
        bm.addMatch(Animal.MONKEY);
        bm.addMatch(Animal.DRAGON);
        bm.addMatch(Animal.RABBIT);
        
        bm.addNoMatch(Animal.HORSE);
        bm.addNoMatch(Animal.GOAT);
        bm.addNoMatch(Animal.DOG);
        
        return bm;
    }
    
    public static AnimalCompatibility getTigerCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.PIG);
        bm.addBestMatch(Animal.DOG);
        bm.addBestMatch(Animal.RABBIT);
        bm.addBestMatch(Animal.HORSE);
        bm.addBestMatch(Animal.ROOSTER);
        
        bm.addMatch(Animal.GOAT);
        bm.addMatch(Animal.RAT);
        bm.addMatch(Animal.OX);
        bm.addMatch(Animal.TIGER);
        bm.addMatch(Animal.DRAGON);
        bm.addMatch(Animal.SNAKE);
        
        bm.addNoMatch(Animal.MONKEY);
        
        return bm;
    }
    
    public static AnimalCompatibility getRabbitCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.PIG);
        bm.addBestMatch(Animal.DOG);
        bm.addBestMatch(Animal.TIGER);
        bm.addBestMatch(Animal.GOAT);
        
        bm.addMatch(Animal.SNAKE);
        bm.addMatch(Animal.MONKEY);
        bm.addMatch(Animal.OX);
        bm.addMatch(Animal.HORSE);
        bm.addMatch(Animal.DRAGON);
        bm.addMatch(Animal.RABBIT);
        
        bm.addNoMatch(Animal.ROOSTER);
        bm.addNoMatch(Animal.RAT);
        
        return bm;
    }
    
    public static AnimalCompatibility getHorseCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.DOG);
        bm.addBestMatch(Animal.TIGER);
        bm.addBestMatch(Animal.GOAT);
        
        bm.addMatch(Animal.DRAGON);
        bm.addMatch(Animal.MONKEY);
        bm.addMatch(Animal.ROOSTER);
        bm.addMatch(Animal.PIG);
        bm.addMatch(Animal.RABBIT);
        bm.addMatch(Animal.SNAKE);
        bm.addMatch(Animal.HORSE);

        bm.addNoMatch(Animal.RAT);
        bm.addNoMatch(Animal.OX);

        return bm;
    }
    
    public static AnimalCompatibility getDragonCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.ROOSTER);
        bm.addBestMatch(Animal.MONKEY);
        bm.addBestMatch(Animal.RAT);
        bm.addBestMatch(Animal.GOAT);
        bm.addBestMatch(Animal.SNAKE);
        
        bm.addMatch(Animal.TIGER);
        bm.addMatch(Animal.PIG);
        bm.addMatch(Animal.OX);
        bm.addMatch(Animal.RABBIT);
        bm.addMatch(Animal.HORSE);
        bm.addMatch(Animal.DRAGON);
        
        bm.addNoMatch(Animal.DOG);
        
        return bm;
    }
    
    public static AnimalCompatibility getSnakeCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.ROOSTER);
        bm.addBestMatch(Animal.MONKEY);
        bm.addBestMatch(Animal.OX);
        bm.addBestMatch(Animal.DRAGON);
        
        bm.addMatch(Animal.RABBIT);
        bm.addMatch(Animal.HORSE);
        bm.addMatch(Animal.TIGER);
        bm.addMatch(Animal.GOAT);
        bm.addMatch(Animal.SNAKE);
        bm.addMatch(Animal.DOG);
        bm.addMatch(Animal.RAT);
        
        bm.addNoMatch(Animal.PIG);
        
        return bm;
    }
    
    public static AnimalCompatibility getGoatCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.HORSE);
        bm.addBestMatch(Animal.RABBIT);
        bm.addBestMatch(Animal.PIG);
        bm.addBestMatch(Animal.DRAGON);
        
        bm.addMatch(Animal.MONKEY);
        bm.addMatch(Animal.GOAT);
        bm.addMatch(Animal.SNAKE);
        bm.addMatch(Animal.ROOSTER);
        bm.addMatch(Animal.RAT);
        bm.addMatch(Animal.DOG);
        bm.addMatch(Animal.TIGER);
        
        bm.addNoMatch(Animal.OX);
        
        return bm;
    }
    
    public static AnimalCompatibility getMonkeyCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.SNAKE);
        bm.addBestMatch(Animal.RAT);
        bm.addBestMatch(Animal.DRAGON);
        
        bm.addMatch(Animal.HORSE);
        bm.addMatch(Animal.GOAT);
        bm.addMatch(Animal.OX);
        bm.addMatch(Animal.ROOSTER);
        bm.addMatch(Animal.PIG);
        bm.addMatch(Animal.DOG);
        bm.addMatch(Animal.MONKEY);
        bm.addMatch(Animal.RABBIT);
        
        bm.addNoMatch(Animal.TIGER);
        
        return bm;
    }
    
    public static AnimalCompatibility getRoosterCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.TIGER);
        bm.addBestMatch(Animal.OX);
        bm.addBestMatch(Animal.DRAGON);
        bm.addBestMatch(Animal.PIG);
        
        bm.addMatch(Animal.DOG);
        bm.addMatch(Animal.GOAT);
        bm.addMatch(Animal.MONKEY);
        bm.addMatch(Animal.HORSE);
        bm.addMatch(Animal.ROOSTER);
        
        bm.addNoMatch(Animal.RABBIT);
        bm.addNoMatch(Animal.RAT);
        
        return bm;
    }
    
    public static AnimalCompatibility getDogCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.RABBIT);
        bm.addBestMatch(Animal.TIGER);
        bm.addBestMatch(Animal.HORSE);
        bm.addBestMatch(Animal.PIG);
        
        bm.addMatch(Animal.RAT);
        bm.addMatch(Animal.SNAKE);
        bm.addMatch(Animal.DOG);
        bm.addMatch(Animal.GOAT);
        bm.addMatch(Animal.MONKEY);
        bm.addMatch(Animal.ROOSTER);
        
        bm.addNoMatch(Animal.DRAGON);
        bm.addNoMatch(Animal.OX);
        
        return bm;
    }
    
    public static AnimalCompatibility getPigCompatibility() {
        AnimalCompatibility bm = new AnimalCompatibility();
        
        bm.addBestMatch(Animal.RABBIT);
        bm.addBestMatch(Animal.TIGER);
        bm.addBestMatch(Animal.ROOSTER);
        bm.addBestMatch(Animal.GOAT);
        bm.addBestMatch(Animal.OX);
        bm.addBestMatch(Animal.DOG);
        
        bm.addMatch(Animal.PIG);
        bm.addMatch(Animal.MONKEY);
        bm.addMatch(Animal.DRAGON);
        bm.addMatch(Animal.RAT);
        bm.addMatch(Animal.HORSE);
        
        bm.addNoMatch(Animal.SNAKE);
        
        return bm;
    }
}

