package ph.extremelogic.common.core.time;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since April 26, 2014
 */

import java.util.Date;

public class ChineseNewYear {
    private Date beginDate;
    private Date endDate;
    private String element;
    private String animal;
    
    public ChineseNewYear() {
        
    }

    /**
     * @param beginDate Begin date.
     * @param endDate End date.
     * @param element Element.
     * @param animal Animal.
     */
    public ChineseNewYear(final Date beginDate,
                          final Date endDate, 
                          final String element,
                          final String animal) {
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.element = element;
        this.animal = animal;
    }
    
    /**
     * @return the beginDate
     */
    public Date getBeginDate() {
        return beginDate;
    }

    /**
     * @param beginDate the beginDate to set
     */
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the element
     */
    public String getElement() {
        return element;
    }

    /**
     * @param element the element to set
     */
    public void setElement(String element) {
        this.element = element;
    }

    /**
     * @return the animal
     */
    public String getAnimal() {
        return animal;
    }

    /**
     * @param animal the animal to set
     */
    public void setAnimal(String animal) {
        this.animal = animal;
    }
}
