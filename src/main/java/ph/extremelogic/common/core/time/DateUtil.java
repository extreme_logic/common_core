package ph.extremelogic.common.core.time;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since April 26, 2014
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public final class DateUtil {
    /**
     * Hidden default constructor.
     */
    private DateUtil() {
        // Intentionally empty.
    }

    /**
     * Days in month of January.
     */
    private static final int DAYS_IN_MON_JAN = 31;

    /**
     * Days in month of February.
     */
    private static final int DAYS_IN_MON_FEB = 28;

    /**
     * Days in month of March.
     */
    private static final int DAYS_IN_MON_MAR = 31;

    /**
     * Days in month of April.
     */
    private static final int DAYS_IN_MON_APR = 30;

    /**
     * Days in month of May.
     */
    private static final int DAYS_IN_MON_MAY = 31;

    /**
     * Days in month of June.
     */
    private static final int DAYS_IN_MON_JUN = 30;

    /**
     * Days in month of July.
     */
    private static final int DAYS_IN_MON_JUL = 31;

    /**
     * Days in month of August.
     */
    private static final int DAYS_IN_MON_AUG = 31;

    /**
     * Days in month of September.
     */
    private static final int DAYS_IN_MON_SEP = 30;

    /**
     * Days in month of October.
     */
    private static final int DAYS_IN_MON_OCT = 31;

    /**
     * Days in month of November.
     */
    private static final int DAYS_IN_MON_NOV = 30;

    /**
     * Days in month of December.
     */
    private static final int DAYS_IN_MON_DEC = 31;

    /**
     * Days in a year.
     */
    public static final int DAYS_IN_YEAR = 365;

    /**
     * Days in a year.
     */
    public static final int DAYS_IN_LEAPYEAR = 366;

    /**
     * Days in a month lookup.
     */
    private static final int[] DAYS_IN_MONTH = {
        DAYS_IN_MON_JAN,
        DAYS_IN_MON_FEB,
        DAYS_IN_MON_MAR,
        DAYS_IN_MON_APR,
        DAYS_IN_MON_MAY,
        DAYS_IN_MON_JUN,
        DAYS_IN_MON_JUL,
        DAYS_IN_MON_AUG,
        DAYS_IN_MON_SEP,
        DAYS_IN_MON_OCT,
        DAYS_IN_MON_NOV,
        DAYS_IN_MON_DEC,
};
    /**
     * @param year Year.
     * @param month Month.
     * @param day Day of the week.
     * @return day of the week representation.
     */
    public static int getDayOfWeek(final int year,
                                   final int month,
                                   final int day) {
//        int[] mo = new int[] { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 }; // monthly offset
//        int af = month > 2 ? 0 : 1; // after february
//        // every fourth year is a leap year, unless it is divisible by 100 unless it is divisible by 400
//        int w = 5 + (year-1700)*365 + (year-1700-af)/4 - (year-1700-af)/100 + (year-1600-af)/400 + mo[month-1] + (day-1);
//        return w % 7;
        Calendar ca1 = new GregorianCalendar();
        ca1.set(year, month - 1, day);

        return ca1.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * Returns true if the year is a leap year in the Gregorian calendar.
     *
     * @param year
     *            The year to consider
     * @return true if <code>year</code> is a leap year
     */
    public static boolean isLeapYear(final int year) {
        return (year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0));
    }

    /**
     * @param referenceDate Reference date from which months will be deducted.
     * @param value Month value.
     * @return Date months subtracted.
     */
    public static Date minusMonths(Date referenceDate, final int value) {
        if (null == referenceDate) {
            referenceDate = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(referenceDate);
        c.add(Calendar.MONTH, -value);

        return c.getTime();
    }

    /**
     * @param min Minimum date
     * @param max Maximum date
     * @param evaluated Date to be evaluated
     * @return true if evaluated is in between
     */
    public static boolean isDateBetween(final Date min,
                                        final Date max,
                                        final Date evaluated) {
        return !evaluated.before(min) && !evaluated.after(max);
    }

    /**
     * @param year Given year.
     * @return Days in a year.
     */
    public static int getDaysInYear(final int year) {
        return isLeapYear(year) ? DAYS_IN_LEAPYEAR : DAYS_IN_YEAR;
    }

    /**
     * @return current year.
     */
    public static int getCurrentYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    /**
     * @return true if current year is a leap year.
     */
    public static boolean isLeapYear() {
        Calendar curCal = Calendar.getInstance();
        int curYear = curCal.get(Calendar.YEAR);

        return isLeapYear(curYear);
    }
    


    /**
     * @param anniv Date of anniversary.
     * @return days until next anniversary.
     */
    public static int daysUntilAnniv(final Date anniv) {
        Calendar curCal = Calendar.getInstance();
        int curMonth = curCal.get(Calendar.MONTH) + 1;
        int curDay = curCal.get(Calendar.DAY_OF_MONTH);
        int curYear = curCal.get(Calendar.YEAR);

        Calendar anvCal = Calendar.getInstance();
        anvCal.setTime(anniv);
        int anvMonth = anvCal.get(Calendar.MONTH) + 1;
        int anvDay = anvCal.get(Calendar.DAY_OF_MONTH);

        int days = 0;

        if (curMonth == anvMonth) {
            if (curDay <= anvDay) {
               days = anvDay - curDay;
            } else {
                int adjustment = 0;
                if (isLeapYear(curYear) || isLeapYear(curYear + 1)) {
                    adjustment = 1;
                }
                days = (365 + adjustment) - (curDay - anvDay);
            }
        } else {
            // Current month is way past the anniversary date still same year.
            if (curMonth > anvMonth) {
                int daysUntil = 0;

                // get remaining days till end of current year
                for (int x = curMonth; x <= 12 ; x++) {
                    daysUntil += getDaysInMonth(x, curYear);
                }

                // but deduct days consumed of current month
                daysUntil -= curDay;

                // get remaining days from first month to month before anniversary month of next year
                for (int x = 1; x < anvMonth ; x++) {
                    daysUntil += getDaysInMonth(x, curYear + 1);
                }
                // then add days of the anniversary month.
                days = daysUntil + anvDay;
            } else {
                int daysUntil = 0;
                for (int x = curMonth; x <= anvMonth; x++) {
                    if (curMonth == x) {
                        daysUntil += (getDaysInMonth(curMonth, curYear) - curDay);
                    } else if (anvMonth == x) {
                        daysUntil += anvDay;
                    } else {
                        int year;
                        if (x > anvMonth) {
                            year = curYear;
                        } else {
                            year = curYear + 1;
                        }
                        daysUntil += getDaysInMonth(x, year);
                    }
                }
                days = daysUntil;
            }
        }
        return days;
    }

    /**
     * @param month Month representation.
     * @param year Year specified.
     * @return Days in a month.
     */
    public static int getDaysInMonth(final int month,
                                     final int year) {
        int adjustment = 0;

        if (isLeapYear(year) && month == Month.FEB) {
            adjustment = 1;
        }

        return DAYS_IN_MONTH[month - 1] + adjustment;
    }

    /**
     * @param month Month to be evaluated.
     * @return int equivalent of the month.
     */
    public static int getMonth(final String month) {
        Locale locale = Locale.getDefault();
        String tmp = month.substring(0, Math.min(month.length(), 3))
                .toUpperCase(locale);
        int mon = 0;

        if ("JAN".equals(tmp)) {
            mon = 1;
        } else if ("FEB".equals(tmp)) {
            mon = 2;
        } else if ("MAR".equals(tmp)) {
            mon = 3;
        } else if ("APR".equals(tmp)) {
            mon = 4;
        } else if ("MAY".equals(tmp)) {
            mon = 5;
        } else if ("JUN".equals(tmp)) {
            mon = 6;
        } else if ("JUL".equals(tmp)) {
            mon = 7;
        } else if ("AUG".equals(tmp)) {
            mon = 8;
        } else if ("SEP".equals(tmp)) {
            mon = 9;
        } else if ("OCT".equals(tmp)) {
            mon = 10;
        } else if ("NOV".equals(tmp)) {
            mon = 11;
        } else if ("DEC".equals(tmp)) {
            mon = 12;
        }
        return mon;
    }

    /**
     * @param year Year.
     * @param month Month.
     * @param day Day of the month.
     * @return true if year, month, and day is valid.
     */
    public static boolean isValidYearMonthDay(final int year,
                                              final int month,
                                              final int day) {
        if ((day == 29) && (month == 2) && isLeapYear(year)) {
            return true;
        }
        if ((month <= 0) || (month > 12)) {
            return false;
        }
        if ((day <= 0) || (day > DAYS_IN_MONTH[month - 1])) {
            return false;
        }
        return true;
    }

    public static String dayOfTheWeek(int code) {
        String day = null;
        switch (code) {
        case DayOfWeek.SUN: day = "Sunday"; break;
        case DayOfWeek.MON: day = "Monday"; break;
        case DayOfWeek.TUE: day = "Tuesday"; break;
        case DayOfWeek.WED: day = "Wednesday"; break;
        case DayOfWeek.THU: day = "Thursday"; break;
        case DayOfWeek.FRI: day = "Friday"; break;
        case DayOfWeek.SAT: day = "Saturday"; break;
        }
        return day;
    }

    /**
     * @param date Date.
     * @param format Format of the date.
     * @return Date object.
     */
    public static Date getDateFromString(final String date,
                                         final String format) {
        Date ret = null;
        try {
            ret = new SimpleDateFormat(format, Locale.ENGLISH).parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ret;
    }

    static class DayOfWeek {
        /**
         * Day of the week representing SUNDAY.
         */
        public static final int SUN = 1;

        /**
         * Day of the week representing MONDAY.
         */
        public static final int MON = 2;

        /**
         * Day of the week representing TUESDAY.
         */
        public static final int TUE = 3;

        /**
         * Day of the week representing WEDNESDAY.
         */
        public static final int WED = 4;

        /**
         * Day of the week representing THURSDAY.
         */
        public static final int THU = 5;

        /**
         * Day of the week representing FRIDAY.
         */

        public static final int FRI = 6;

        /**
         * Day of the week representing SATURDAY.
         */
        public static final int SAT = 7;
    }

    static class Month {
        /**
         * Month constant for January.
         */
        public static final int JAN = 1;

        /**
         * Month constant for February.
         */
        public static final int FEB = 2;

        /**
         * Month constant for March.
         */
        public static final int MAR = 3;

        /**
         * Month constant for April.
         */
        public static final int APR = 4;

        /**
         * Month constant for May.
         */
        public static final int MAY = 5;

        /**
         * Month constant for Jun.
         */
        public static final int JUN = 6;

        /**
         * Month constant for July.
         */
        public static final int JUL = 7;

        /**
         * Month constant for August.
         */
        public static final int AUG = 8;

        /**
         * Month constant for September.
         */
        public static final int SEP = 9;

        /**
         * Month constant for October.
         */
        public static final int OCT = 10;

        /**
         * Month constant for November.
         */
        public static final int NOV = 11;

        /**
         * Month constant for December.
         */
        public static final int DEC = 12;
    }
}
