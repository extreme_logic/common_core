package ph.extremelogic.common.core.time.chinese;

import java.util.ArrayList;

public class AnimalCompatibility {
    public AnimalCompatibility() {
        mBestMatch = new  ArrayList <String>();
        mMatch = new  ArrayList <String>();
        mNoMatch = new  ArrayList <String>();
    }
    


    public void addBestMatch(String animal) {
        mBestMatch.add(animal);
    }
    public void addMatch(String animal) {
        mMatch.add(animal);
    }
    public void addNoMatch(String animal) {
        mNoMatch.add(animal);
    }
    private ArrayList <String>mBestMatch;
    
    public ArrayList<String> getBestMatch() {
        return mBestMatch;
    }



    public ArrayList<String> getMatch() {
        return mMatch;
    }



    public ArrayList<String> getNoMatch() {
        return mNoMatch;
    }
    private ArrayList <String>mMatch;
    private ArrayList <String>mNoMatch;
}
