/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ph.extremelogic.common.core.random;

/*
 * Copyright (C) 2020 Extreme Logic Ph.
 * https://www.extremelogic.ph
 *
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since August 7, 2020
 */

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public final class RandomThings {
    private RandomThings() { }

    /**
     * Maximum digit.
     */
    private static final int MAX_DIGIT = 9;

    /**
     * Minimum digit.
     */
    private static final int MIN_DIGIT = 0;

    /**
     * Maximum number for alphabet.
     */
    private static final int MAX_ALPHA_NUMBER = 25;

    /**
     * Random digit.
     * @return random digit range is 0 to 9
     */
    public static int digit() {
        return numberBetween(MIN_DIGIT, MAX_DIGIT);
    }

    /**
     * Random letter.
     * @return random letter case insensitive
     */
    public static char letter() {
        return bit() == 1 ? bigLetter() : smallLetter();
    }

    /**
     * Random big letter.
     * @return random big letter
     */
    public static char bigLetter() {
        return (char) ((int) 'A' + numberBetween(0, MAX_ALPHA_NUMBER));
    }

    /**
     * Random small letter.
     * @return random small letter
     */
    public static char smallLetter() {
        return (char) ((int) 'a' + numberBetween(0, MAX_ALPHA_NUMBER));
    }

    /**
     * Random string with length N.
     * @param length Length of the string
     * @return String with length N
     */
    public static String string(final int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int x = 0; x < length; x++) {
            sb.append(letter());
        }
        return sb.toString();
    }

    /**
     * Random number with length N.
     * @param length Length of the number
     * @return Number with length N
     */
    public static String number(final int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int x = 0; x < length; x++) {
            sb.append(digit());
        }
        return sb.toString();
    }

    /**
     * Random item from list.
     * @param given Given list
     * @param <T> Type of object in the list
     * @return Random item from list
     */
    public static <T> T fromList(final List<T> given) {
        if (null == given || given.size() == 0) {
            return null;
        }
        int max = given.size() - 1;
        return given.get(numberBetween(0, max));
    }

    /**
     * Random item from list.
     * @param given Given list
     * @param used Items to ignore from the given list
     * @param <T> Type of object in the list
     * @return Random item from list
     */
    public static <T> T fromList(final List<T> given, final List<T> used) {
        List<T> selection = given;
        selection.removeAll(used);
        return fromList(selection);
    }

    /**
     * Random bit.
     * @return bit 1 or 0
     */
    public static int bit() {
        return numberBetween(0, 1);
    }

    /**
     * Random number between min and max. Swaps if min and max is wrong.
     * @param min Minimum number accepted
     * @param max Maximum number accepted
     * @return Number in range min/max
     */
    public static int numberBetween(final int min, final int max) {
        int x = max;
        int n = min;
        if (n > x) {
            x ^= n;
            n ^= x;
            x ^= n;
        }
        return ThreadLocalRandom.current().nextInt(n, x + 1);
    }

    /**
     * Random boolean.
     * @return true or false
     */
    public static boolean bool() {
        return bit() == 1 ? true : false;
    }
}
