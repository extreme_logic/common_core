/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ph.extremelogic.common.core.random;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since October 17, 2018
 */

import java.util.Random;

/**
 * Class that contains various methods that utilizes random things.
 */
public class RandomUtil {
	
	/**
	 * Initializes seed.
	 */
	public RandomUtil() {
		seed = new Random();
	}

	/** Seed variable */
	private static Random seed;

	/**
	 * Generates a random number between min and max.
	 * @param min Minimum number.
	 * @param max Maximum number.
	 * @return Random number generated based on parameter input.
	 */
	public int getRandomNumber(int min, int max) {
		if (min >= max) {
			throw new IllegalArgumentException("Max must be greater than min");
		}

		return seed.nextInt((max - min) + 1) + min;
	}

	/**
	 * @return Random boolean
	 */
	public boolean getRandomBoolean() {
		return (1 == getRandomNumber(0, 1)) ? true : false;
	}
	/**
	 * @return Random small letter.
	 */
	public char getRandomSmallLetter() {
		return (char) ((int) 'a' + getRandomNumber(0, 25));
	}

	/**
	 * @return Random big letter.
	 */
	public char getRandomBigLetter() {
		return (char) ((int) 'A' + getRandomNumber(0, 25));
	}

	/**
	 * @return Random letter.
	 */
	public char getRandomLetter() {
		if (getRandomBoolean()) {
			return getRandomSmallLetter();
		} else {
			return getRandomBigLetter();
		}
	}

	/**
	 * @param length Length of the string.
	 * @return Random string.
	 */
	public String getRandomString(int length) {
		StringBuffer sb = new StringBuffer(length);
		for (int x = 0; x < length; x++) {
			sb.append(getRandomLetter());
		}
		return sb.toString();
	}
}
