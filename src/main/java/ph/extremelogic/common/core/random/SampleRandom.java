package ph.extremelogic.common.core.random;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since October 17, 2018
 */

/**
 * Sample execution.
 */
public class SampleRandom {
	public static void main(String[] args) {

		RandomUtil r = new RandomUtil();
		// TODO Auto-generated method stub
		for (int x = 0; x < 26; x++) {
			System.out.println(x + ") " + r.getRandomString(5));
		}
	}
}